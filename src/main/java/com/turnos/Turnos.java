package com.turnos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Turnos {

	public static void main(String[] args) {
		SpringApplication.run(Turnos.class, args);
	}
	
//	@Bean
//	public CommandLineRunner initData (IUserRepository userRepository) {
//		return (args)->{
//			System.out.println(userRepository.save(new Usuario("tomas.pereyra",new BCryptPasswordEncoder().encode("123"))));
//			System.out.println(userRepository.findByEmail("tomas.pereyra"));
//			};
//	}


}
