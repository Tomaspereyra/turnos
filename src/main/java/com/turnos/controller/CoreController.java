package com.turnos.controller;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turnos.model.Comercio;
import com.turnos.model.Dueño;
import com.turnos.service.IComercioService;

@RestController
@CrossOrigin(origins = "http://localhost:4200") //  the annotation enables Cross-Origin Resource Sharing (CORS) on the server.
public class CoreController {
	@Autowired
	private IComercioService comercioService;
	@RequestMapping("/registro")
    @PostMapping
	public void registro(@RequestBody Map<String,String> comercio) {
		// agregar un mapper y desacoplar 
		Comercio com = new Comercio();
		com.setNombreComercio(comercio.get("nombreComercio"));
		com.setUsuario(comercio.get("usuarioComercio"));
		com.setCreateAt(new Date());
		Dueño dueño = new Dueño();
		dueño.setApellido(comercio.get("apellidoPF"));
		dueño.setNombre(comercio.get("nombrePF"));
		dueño.setMail(comercio.get("emailPF"));
		com.setDueño(dueño);
		
		this.comercioService.registrarComercio(com);
		
	}

}
