package com.turnos.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.turnos.model.Comercio;
import com.turnos.repository.IComercioRepository;
import com.turnos.service.IComercioService;

@Service
public class ComercioServiceImpl implements IComercioService {
	@Autowired
	private IComercioRepository comercioRepository;
	public ComercioServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Comercio registrarComercio(Comercio comercio) {
		Comercio com = this.comercioRepository.save(comercio);
		return com;
	}

}
