package com.turnos.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="comercios")
public class Comercio {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String nombreComercio;
	@Column(name="usuario",unique = true,nullable = false)
	private String usuario;
	
	@Column(name="create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="dueño_id",referencedColumnName="id")
	private Dueño dueño;

	public Comercio() {
		// TODO Auto-generated constructor stub
	}
	
	
	

	public String getUsuario() {
		return usuario;
	}




	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}




	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getNombreComercio() {
		return nombreComercio;
	}



	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}



	public Date getCreateAt() {
		return createAt;
	}



	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}



	public Dueño getDueño() {
		return dueño;
	}



	public void setDueño(Dueño dueño) {
		this.dueño = dueño;
	}



	@Override
	public String toString() {
		return "Comercio [id=" + id + ", nombreComercio=" + nombreComercio + ", createAt=" + createAt + ", dueño="
				+ dueño + "]";
	}
	

}
