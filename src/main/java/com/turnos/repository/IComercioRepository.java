package com.turnos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.turnos.model.Comercio;

public interface IComercioRepository extends JpaRepository<Comercio,Integer>{

	
}
